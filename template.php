<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\FrontendAsset;
use common\widgets\Alert;

FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="assets/owl-carousel/assets/owl.carousel.css">
<script src="assets/owl-carousel/owl.carousel.min.js"></script>

</head>
<body>
<?php $this->beginBody() ?>
  <header class="main-head">
    <nav class="top-line">
      <div class="container">
        <ul>
          <li><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> Задать вопрос</a></li>
          <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Личный кабинет / Регистрация</a></li>
          <li><a href="#">Каз</a></li>
        </ul>
        </div>
    </nav>
  </header>
  <section class="tx-panel">
    <div class="row">
      <div class="col-md-2">
        <nav class="left-mnu">
        <ul class="main-mnu">
          <li><a href="sample.html">Поиск по ТН ВЭД коду</a></li>
          <li><a href="#">Информация для заявителей</a></li>
            <ul class="sub-menu">
              <li><a href="">Сертификация продукции и услуг</a></li>
              <li><a href="">Испытательная лаборатория</a></li>
              <li><a href="">Сертификация менеджмента ISO 9001, 14000 и т.д</a></li>
            </ul>
          <li><a href="#">О компании</a></li>
          <li><a href="#">Контакты</a></li>
        </ul>
    </nav>
      </div>
      <div class="col-md-9">
        <?= $content ?>
      </div>
      <div class="col-md-1">
      </div>
    </div>
  </section>
  <footer class="footer">
  <div class="col-md-8 col-md-offset-2">
    <div class="container">
       <div class="pull-left"><p>&copy; Центр Сертификации <?= date('Y') ?></p></div> <a href="#">Правила использования</a> <a href="#">Конфиденциальность</a> 
    </div>
  </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>