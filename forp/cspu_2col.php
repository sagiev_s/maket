<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainAsset;
use common\widgets\Alert;
use yii\widgets\Menu;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
  <section class="mhead">
    <div class="main-head">
    <div class="container-fluid">
              <div class="row">
          <div class="off-head">&nbsp;</div>
          <div class="mnu-head">
                <?php
                echo Menu::widget([
              'items' => [
                ['label' => '<img class="menuicon1" src="img/rectangle-5.svg"> Задать вопрос', 'url' => ['site/ask']],
                ['label' => '<img class="menuicon2" src="img/combined-shape.svg"> Личный кабинет / Регистрация', 'url' => ['site/login']],
                ['label' => '<img class="menuicon3" src="img/k-z.svg" alt=""> Каз', 'url' => ['site/kz']]
              ],
              'options' => ['class' =>'top-mnu'], // set this to nav-tab to get tab-styled navigation
              'encodeLabels' => false,
              ]);
              ?>
              </div>
            </div>
          </div>
    </div>
    <div class="logo-wrap">
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><hr class="redline"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 logos"><img src="img/cspulogo_upd.png" class="mainlogo"><hr class="blueline"></div>
        </div>
      </div>
    </div>
    <div class="header-wrap">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 col-xs-12 col-md-2 col-sm-2">
            <div class="sidebar">
              <?php
                echo Nav::widget([
              'items' => [
                ['label' => 'Поиск по ТН ВЭД коду', 'url' => ['site/tree']],
                ['label' => 'О компании', 'url' => ['site/about']],
                ['label' => 'Услуги', 'url' => ['site/services']],
                ['label' => 'Информация для заявителей', 'url' => ['site/info']],
                ['label' => 'Контакты', 'url' => ['site/contact']],
              ],
              'options' => ['class' =>'sidebar-mnu'], // set this to nav-tab to get tab-styled navigation
              ]);
              ?>
              <div class="call">
                <p>У вас есть вопросы? <br> Позвоните нам!</p>
                <h1>+7 (555) 33 44 22</h1>
              </div>
            </div>
          </div>
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <?= $content ?>
          </div>
        </div>  
      </div>
    </div>
  </section>
  <footer class="footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
        <div class="col-lg-9 col-md-9 col-sm-9">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="copyright">
                <p>© Центр Сертификации 2016</p>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="useterm">
                <a href="">Правила использования</a><a href="">Конфиденциальность</a>
              </div>
            </div>
          </div>
        </div>
  </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>