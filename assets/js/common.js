$(function() {
	var owl = $(".slider");
	owl.owlCarousel({
		loop: true,
		items: 1,
		itemClass: "slide-wrap",
		nav: true,
		navText: ""
	});
	$(".owl-prev").click(function(){
		owl.trigger('prev.owl.carousel');
	})
	$(".owl-next").click(function(){
		owl.trigger('next.owl.carousel');
	})
});
